# SentinetAImod
Minecraft characters powered by AI. Inspired by OASIS from 'Ready Player One' and 'The Wayfarer' series from Becky Chambers. For background see https://hlgr360.github.io/blog/blog/sentinent-ai-minecraft/.

## Development Setup (on MacOS)
* Download and install Java JRE 1.8 from https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
* Download and install Eclipse IDE from https://www.eclipse.org/downloads/
* Download the Forge SDK (in this case 11.2 - pick the MDK version) from http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.11.2.html
  * You should now have a 'forge' directory in your 'Downloads' folder.  Move this folder to your project directory.
  * Optionally: Edit the 'gradlew' file in the 'forge' directory.  Change the DEFAULT_JVM_OPTS="" line to DEFAULT_JVM_OPTS="-Xmx1024m"
  * Run the following commands in the 'forge' directory (see https://techwiseacademy.com/minecraft-modding-setting-up-your-environment/)
    * `./gradlew setupDecompWorkspace --refresh-dependencies` 
    * `./gradlew setupDecompWorkspace eclipse`
    * Hint: To rebuild the workspace and dependencies precede with the following commands
      * `gradlew clean`
      * `gradlew cleanCache`
* Clone the 'SentinentAI' mod repo into a local directory (do not clone it into the 'forge' directory) 
* Hint: The following setup is based on the project setup from BedrockMiner from https://bedrockminer.jimdo.com/modding-tutorials/set-up-minecraft-forge/set-up-advanced-setup/
* Start Eclipse and create a new workspace in your project directory (do not (re)use the 'forge' workspace)
* Import Forge into your workspace (File > Import and choose 'Existing Projects into workspace') 
  * In the dialog for the import click the "Browse" button and select your Forge directory as the source
  * Ensure that a project named "Forge" is in the list of projects and is checked. If so, press OK, otherwise you probably choose a wrong directory.
  * By performing the import, Eclipse linked the Forge workspace to your mod's workspace. You now have access to Forge in your workspace.
* Setting up the SentinentAI mod project
  * Create your mod project (File > New > Project and select "Java Project")
  * Remove the empty 'src' directory
  * Link the 'src' directory of the cloned mod repo into your Eclipse mod project (File > New > Folder)
    * Select the Eclipse mod project as parent folder
    * Unfold the advanced tab and select 'Link to alternate location'
    * Browse to the location of the local 'SentinentAI/src' directory and add it
  * Unfold the newly added linked 'src' directory and ctrl-click the 'src/main/java' directory
  * Select as 'Use As Source Folder' from the 'Build Path' submenu
  * Repeat the above two steps for the 'src/main/resources' directory
* Add Forge as build dependency to SentinentAI
  * Ctrl-click on the SentinentAI mod project in the Package Explorer on the left. Click on 'Build Path > Configure'. 
  * Go to the tab "Projects" and click "Add" and select Forge as the project and click OK.
* Adding Run Configurations
  * Select the mod project in the Package Explorer and click on the drop-down arrow next to the green 'Run' button in the top tool bar. 
  * In the drop-down menu click on 'Run Configurations', right-click on 'Java Application' and click 'Add'.
  * Create a 'Run Client' configuration using `GradleStart` as the Main Class 
  * Selecting "Run", Eclipse should start compiling the mod and start the Minecraft client with Forge and SentinentAI installed
  * Hint: If you get a build error, try to explicitly import the 'guava' jar into your build config
    * Ctrl-click on the SentinentAI mod project in the Package Explorer on the left. Click on 'Build Path > Configure'. 
    * Go to the tab "Libraries" and click "Add External JAR"
    * Navigate to the guava.jar in '<home>/.gradle/caches (i.e. like '<home>/.gradle/caches/modules-2/files-2.1/com.google.guava/guava/18.0/cce0823396aa693798f8882e64213b1772032b09')
  * Create a 'Run Server' configuration using `GradleStartServer` as the Main Class
  * Selecting "Run", Eclipse should start compiling the mod and start the Minecraft server with Forge and SentinentAI installed
